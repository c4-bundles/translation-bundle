<?php

namespace C4\TranslationBundle\Admin;

use Knp\Menu\ItemInterface as MenuItemInterface;
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Admin\AdminInterface;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Symfony\Component\DependencyInjection\ContainerInterface;

class TranslationAdmin extends Admin
{
    protected $contexts;
    protected $types;
    protected $maxPerPage = 400;

    protected $datagridValues = ['_sort_order' => 'DESC'];

    public function __construct($code, $class, $baseControllerName, ContainerInterface $container)
    {
        parent::__construct($code, $class, $baseControllerName);

        $this->contexts = $container->getParameter('c4.translation.contexts');
        $this->types = $container->getParameter('c4.translation.types');
    }

    protected function configureListFields(ListMapper $listMapper) {
        $listMapper
            ->addIdentifier('key')
            ->add('lvTranslation')
            ->add('ruTranslation')
            ->add('enTranslation')
            ->add('context', 'choice', ['choices' => $this->contexts])
            ->add('type', 'choice', ['choices' => $this->types])
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('context', 'doctrine_orm_choice', [], 'choice', ['choices' => $this->contexts])
            ->add('type', 'doctrine_orm_choice', [], 'choice', ['choices' => $this->types])
            ->add('key')
            ->add('lvTranslation', null, ["label" => "In Latvian"])
            ->add('ruTranslation', null, ["label" => "In Russian"])
            ->add('enTranslation', null, ["label" => "In English"])
        ;
    }
    public function getFilterParameters()
    {
        $this->datagridValues = array_merge(array(
            'context' => array(
                'value' => 'fe',
            )
        ),$this->datagridValues);

        return parent::getFilterParameters();
    }

    protected function configureFormFields(FormMapper $formMapper) {
        $formMapper
            ->with('Configuration', ['class' => 'col-md-6'])
                ->add('context', 'choice', ['choices' => $this->contexts])
                ->add('type', 'choice', ['choices' => $this->types])
                ->add('key')
                ->add('lvTranslation', null, ['required' => false])
                ->add('ruTranslation', null, ['required' => false])
                ->add('enTranslation', null, ['required' => false])
            ->end()
        ;
    }

    protected function configureSideMenu(MenuItemInterface $menu, $action, AdminInterface $childAdmin = null)
    {
        if ($action == 'list') {
            $admin = $this->isChild() ? $this->getParent() : $this;

            $menu->addChild(
                'Generate translation files (fe)',
                ['uri' => $admin->generateUrl('generateTranslationFiles', [
                    'context' => 'fe'
                ])]
            );

            $menu->addChild(
                'Generate translation files (cms)',
                ['uri' => $admin->generateUrl('generateTranslationFiles', [
                    'context' => 'cms'
                ])]
            );
        }
    }

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->add('generateTranslationFiles', 'generate/translation-files/{context}');
        $collection->remove("delete");
        $collection->remove('create');
    }

    public function getBatchActions()
    {
        $actions = parent::getBatchActions();
        unset($actions['delete']);

        return $actions;
    }
}
