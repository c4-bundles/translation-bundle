<?php

namespace C4\TranslationBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\HttpKernel\Kernel;

class TranslationController extends Controller
{
    public function getTranslationsAction($context = "fe", $lang = "en", $action = "get")
    {
        $kernel = $this->container->get('kernel');
        $basepath = dirname($kernel->getRootDir());

        if ($context == "fe") {
            $file = $kernel->locateResource("@FrontendBundle/Resources/translations/FrontendBundle.$lang.yml");
        }
        if ($context == "cms") {
            $file = "$basepath/app/Resources/translations/messages.$lang.yml";
        }

        // check if file exists
        $fs = new Filesystem();
        if (!$fs->exists($file)) {
            throw $this->createNotFoundException();
        }

        // prepare BinaryFileResponse
        $response = new BinaryFileResponse($file);
        $response->trustXSendfileTypeHeader();
        $response->setContentDisposition(
            ($action == "get") ? ResponseHeaderBag::DISPOSITION_INLINE : ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            basename($file),
            iconv('UTF-8', 'ASCII//TRANSLIT', basename($file))
        );

        return $response;
    }

    public function getParentDir($dir)
    {
        return dirname($dir);
    }

}
