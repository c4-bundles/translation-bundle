<?php

namespace C4\TranslationBundle\Controller;

use Sonata\AdminBundle\Controller\CRUDController as Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Yaml\Yaml;
use C4\TranslationBundle\Entity\Translation;

use Symfony\Component\Filesystem\Filesystem;

class TranslationAdminController extends Controller
{

    private $repository;
    private $em;

    public function generateTranslationFilesAction($context)
    {

        ini_set('max_execution_time', 3000); //300 seconds = 5 minutes
        set_time_limit(3000);

//        $path = $this->getParameter("c4.translation.contexts.$context.path");
        $rootDir = $this->getParameter('kernel.root_dir');
        if ($context == "fe") {
            $filepath = "$rootDir/../src/C4/FrontendBundle/Resources/translations";
            $filename = "FrontendBundle";
        } else {
            $filepath = "$rootDir/Resources/translations";
            $filename = "messages";
        }

        $languages = [
            'lv' => [],
            'ru' => [],
            'en' => [],
        ];

        // insert translation bundle messages
        $this->em = $this->getDoctrine()->getManager();
        $this->repository = $this->getDoctrine()->getRepository('TranslationBundle:Translation');

        // update db from messages before overwrite
        $this->em = $this->getDoctrine()->getManager();
        $this->repository = $this->getDoctrine()->getRepository('TranslationBundle:Translation');
        foreach ($languages as $lang => $tmp) {
            if (!file_exists("$filepath/$filename.$lang.yml")) {
                continue;
            }
            $messages = yaml::parse(file_get_contents("$filepath/$filename.$lang.yml"));
            if ($messages) {
                foreach ($messages as $key => $val) {
                    if (is_array($val)) {
                        foreach ($this->arrayToScalar($key, $val) as $k => $v) {
                            $this->check_database($context, $k, $v, $lang);
                        }
                    } else {
                        $this->check_database($context, $key, $val, $lang);
                    }
                }
            }
        }

        $translations = $this->getDoctrine()->getRepository('TranslationBundle:Translation')->findBy(
            ['context' => $context],
            ['key' => 'ASC']
        );

        foreach ($translations as $translation) {
            $key = $translation->getKey();

            if ($lv_translation = $translation->getLvTranslation()) {
                //$this->scalarToArray($languages['lv'], $key, $lv_translation);

                if ($lv_translation) {
                    $languages['lv'][$key] = $lv_translation;
                }
            }

            if ($ru_translation = $translation->getRuTranslation()) {
                //$this->scalarToArray($languages['ru'], $key, $ru_translation);

                if ($ru_translation) {
                    $languages['ru'][$key] = $ru_translation;
                }
            }

            if ($en_translation = $translation->getEnTranslation()) {
                //$this->scalarToArray($languages['en'], $key, $en_translation);

                if ($en_translation) {
                    $languages['en'][$key] = $en_translation;
                }
            }
        }

        foreach ($languages as $language => $translations) {
            if ($translations) {
                $yaml = Yaml::dump($translations, 10);

                file_put_contents("$filepath/$filename.$language.yml", $yaml);
            }
        }

        $fs = new Filesystem();
        $fs->remove($this->container->getParameter('kernel.cache_dir'));

        $this->addFlash(
            'sonata_flash_success',
            "Translation files ($context) has been successfully generated."
        );

        return new RedirectResponse($this->admin->generateUrl('list'));
    }

    private function scalarToArray(&$arr, $path, $value)
    {
        $keys = explode('.', $path);

        foreach ($keys as $key) {
            // todo - error???
//            dump([$arr, $path, $value, $keys, $key]);
            try {
                $arr = &$arr[$key];
            } catch (\Exception $e) {
//                dump([$arr, $path, $value, $keys, $key]);
//                die();
                continue;
            }
        }

        $arr = $value;
    }

    private function arrayToScalar($key, $val)
    {
        $result = [];
        foreach ($val as $k => $v) {
            $str_key = "$key.$k";
            if (!is_array($v)) {
                $result[$str_key] = $v;
            } else {
                $result = array_merge($result, $this->arrayToScalar($str_key, $v));
            }
        }

        return $result;
    }

    private function check_database($context, $key, $val, $lang)
    {
        // search
        $db_translation = $this->repository->createQueryBuilder('t')
            ->where("t.context = '$context'")
//            ->andWhere("t.type = 'cms'")
            ->andWhere("t.key = '$key'")
            ->setMaxResults(1)->getQuery()->getOneOrNullResult();
        // insert
        if (!$db_translation) {
            $translation = new Translation();

            $translation->setContext($context);
            $translation->setType("text");
            $translation->setKey("$key");

            $fn = "set".ucfirst(strtolower($lang))."Translation";
            $translation->$fn($val);

            $this->em->persist($translation);
            $this->em->flush();
        } else {
            $fn = "get".ucfirst(strtolower($lang))."Translation";
            if ($db_translation->$fn() == "") {
                $fn = "set".ucfirst(strtolower($lang))."Translation";
                $db_translation->$fn($val);
                $this->em->persist($db_translation);
                $this->em->flush();
            }
        }
    }
}
