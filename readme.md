Installation
------------

1. uncomment app/config/config.yml line:  
    ```translator: { fallbacks: ['%locale%'] }```

2. add to app/AppKernel.php into $bundles array:
new C4\TranslationBundle\TranslationBundle(),

3. must be created or installed FrontendBundle

4. must have folder FrontendBundle/Resources/translations/ (frontend translations) // todo - need better solution

5. must have folder /app/Resources/translations/ (cms translations)

6. must have sonata-admin installed // todo - maybe we create version that not requires sonata admin?

Configuration
-------------

- /app/config/parameters.yml
```
parameters:
    translate_cms = true/false
```

- /app/config/routing.yml
```
TranslationBundle:
    resource: "@TranslationBundle/Resources/config/routing.yml"
    prefix:   /
```

TODO
----
- infinite language support