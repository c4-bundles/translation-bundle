<?php
namespace C4\TranslationBundle\Profiler\DataCollector;

use Symfony\Component\HttpKernel\DataCollector\DataCollector;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\ORM\EntityManager;

use C4\TranslationBundle\Components\Translator;
use C4\TranslationBundle\Entity\Translation;

class TranslationDataCollector extends DataCollector
{

    private $container;
    private $em;
    private $translator;
    public $data = [];

    public function __construct(ContainerInterface $container, EntityManager $entityManager, Translator $translator)
    {
        $this->container = $container;
        $this->em = $entityManager;
        $this->translator = $translator;
    }

    public function getName()
    {
        return 'c4.translation_data_collector';
    }

    public function collect(Request $request, Response $response, \Exception $exception = null)
    {
//        $profile = $this->container->get('profiler');
//        $t = $profile->get("translation");
//        $t->lateCollect();
//        $this->data["defined"] = $t->getCountDefines();
//        $this->data["missing"] = $t->getCountMissings();
//        $this->data["fallback"] = $t->getCountFallbacks();
//        $this->data["translations"] = $t->getMessages();

        $this->data["translations"] = $this->translator->translations;
        
        $this->data["translations_found"] = $this->translator->translations_found;
        $this->data["translations_fallback"] = $this->translator->translations_fallback;
        $this->data["translations_missing"] = $this->translator->translations_missing;

//        $r = $this->em->getRepository('TranslationBundle:Translation');
//        $rows = $r->createQueryBuilder('t')->getQuery()->getResult();
//        $this->data["in_database"] = count($rows);

//        $this->fix_missing_translations();
    }

    public function fix_missing_translations()
    {
        $r = $this->em->getRepository("TranslationBundle:Translation");

        foreach ($this->translator->missing_translations as $id => $trans)
        {

            // search
            $db_translation = $r->createQueryBuilder('t')
                ->where("t.context = 'cms'")
                ->andWhere("t.type = 'cms'")
                ->andWhere("t.key = '$id'")
                ->setMaxResults(1)->getQuery()->getOneOrNullResult();
            // insert
            if (!$db_translation)
            {
                $translation = new Translation();

                $translation->setContext("cms");
                $translation->setType("cms");
                $translation->setKey("$id");

                $translation->setEnTranslation(($trans["locale"] == "en") ? $trans["translation"] : $this->google_translate($trans["translation"], $trans["locale"], "en"));
                $translation->setLvTranslation(($trans["locale"] == "lv") ? $trans["translation"] : $this->google_translate($trans["translation"], $trans["locale"], "lv"));
                $translation->setRuTranslation(($trans["locale"] == "ru") ? $trans["translation"] : $this->google_translate($trans["translation"], $trans["locale"], "ru"));

                $this->em->persist($translation);
                $this->em->flush();
            }
        }
    }

    public function google_translate($msg, $from_locale, $to_locale)
    {

        $apiKey = $this->container->getParameter('google_api_key');
        $google_translations = $this->container->getParameter('google_translations');

        if ($apiKey == '' || $google_translations != "on") return "";

        $text = $msg;
        $url = 'https://www.googleapis.com/language/translate/v2?key=' . $apiKey . '&q=' . rawurlencode($text) . '&source=' . $from_locale . '&target=' . $to_locale;

        $handle = curl_init($url);
        curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($handle);
        $responseDecoded = json_decode($response, true);
        curl_close($handle);

//        echo 'Source: ' . $text . '<br>';
//        echo 'Translation: ' . $responseDecoded['data']['translations'][0]['translatedText'];
        $responseDecoded['data']['translations'][0]['translatedText'];
    }

}
