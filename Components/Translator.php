<?php namespace C4\TranslationBundle\Components;

use Symfony\Component\Finder\Finder;

//use Symfony\Component\Translation\MessageSelector;
//use Symfony\Component\DependencyInjection\ContainerInterface;
//
use Symfony\Bundle\FrameworkBundle\Translation\Translator as SymfonyTranslator;
//
use Symfony\Component\Yaml\Yaml;
use C4\TranslationBundle\Entity\Translation;
use Symfony\Component\Filesystem\Filesystem;

use Psr\Container\ContainerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface as SymfonyContainerInterface;
use Symfony\Component\HttpKernel\CacheWarmer\WarmableInterface;
use Symfony\Component\Translation\Translator as BaseTranslator;
use Symfony\Component\Translation\MessageSelector;
use Symfony\Component\Translation\Exception\InvalidArgumentException;

class Translator extends SymfonyTranslator
{
    private $symfonyContainer;
    private $selector;
    public $translator;

    public $translation = [];
    public $translations = [];

    public $translations_found = [];
    public $translations_missing = [];
    public $translations_fallback = [];

    public function __construct(ContainerInterface $container, MessageSelector $selector, $defaultLocale = null, array $loaderIds = [], array $options = [], SymfonyContainerInterface $symfonyContainer)
    {

        $this->symfonyContainer = $symfonyContainer;
        parent::__construct($container, $selector, $defaultLocale, $loaderIds, $options);
        $this->selector = $selector ?: new MessageSelector();
    }

    public function trans($id, array $parameters = [], $domain = null, $locale = null)
    {

        if (isset($this->translations[$id])) {
            $this->translations[$id]["used"]++;

            return $this->translations[$id]["translation"];
        }

        $catalogue = $this->getCatalogue($locale);
        $locale = $catalogue->getLocale();

        $default = "";
        if (isset($parameters["default"])) {
            $default = $parameters["default"];
            unset($parameters["default"]);
        }
        $this->translation = [
            "key"         => $id,
            "translation" => "",
            "default"     => $default,
            "found"       => false,
            "search_in"   => ($domain) ?: "",
            "found_in"    => "",
            "used"        => 1,
        ];

        $has_translation = false;

        // if domain not specified - set to messages, if translation found in messages
        if (null === $domain) {
            if ($this->symfonyContainer->hasParameter('translate_cms') && $this->symfonyContainer->getParameter('translate_cms') != false) {
                $has_translation = $this->checkTranslation($id, $locale, "messages", $default);
                if ($has_translation) {
                    $domain = 'messages';
                }
            }
        } // if frontend, set to FrontendBundle
        elseif ($domain == "frontend") {
            $domain = "FrontendBundle";
            $has_translation = $this->checkTranslation($id, $locale, $domain, $default);
        } // if domain specified, check if translation is in domain or try in messages
        else {
            if ($this->symfonyContainer->hasParameter('translate_cms') && $this->symfonyContainer->getParameter('translate_cms') != false) {
                $has_translation = $this->checkTranslation($id, $locale, $domain, $default);
                if (!$has_translation) {
                    $domain = 'messages';
                    $has_translation = $this->checkTranslation($id, $locale, $domain, $default);
                }
            }
        }

        if (!$has_translation && $default) {
            $translated = $default;
        } else {
            $translated = strtr($catalogue->get((string)$id, $domain), $parameters);
        }

        $this->translation["found"] = $has_translation;
        $this->translation["found_in"] = $domain;

        $this->translation["translation"] = $translated;
        $this->translations[$id] = $this->translation;

        return $translated;
    }

    public function transChoice($id, $number, array $parameters = [], $domain = null, $locale = null)
    {
        // todo : different parameters?
//        if (isset($this->translations[$id])){
//            $this->translations[$id]["used"]++;
//            return $this->translations[$id]["translation"];
//        }

        $catalogue = $this->getCatalogue($locale);
        $locale = $catalogue->getLocale();

        $default = null;
        if (isset($parameters["default"])) {
            $default = $parameters["default"];
            unset($parameters["default"]);
        }

        $this->translation = [
            "key"         => $id,
            "translation" => "",
            "default"     => $default,
            "found"       => false,
            "search_in"   => ($domain) ?: "",
            "found_in"    => "",
            "used"        => 1,
        ];

        $has_translation = false;

        // if domain not specified - set to messages, if translatin found in messages
        if (null === $domain) {
            if ($this->symfonyContainer->hasParameter('translate_cms') && $this->symfonyContainer->getParameter('translate_cms') != false) {
                $has_translation = $this->checkTranslation($id, $locale, "messages", $default);
                if ($has_translation) {
                    $domain = 'messages';
                }
            }
        } // if frontend, set to FrontendBundle
        elseif ($domain == "frontend") {
            $domain = "FrontendBundle";
            $has_translation = $this->checkTranslation($id, $locale, $domain, $default);
        } // if domain specified, check if translation is in domain or try in messages
        else {
            if ($this->symfonyContainer->hasParameter('translate_cms') && $this->symfonyContainer->getParameter('translate_cms') != false) {
                $has_translation = $this->checkTranslation($id, $locale, $domain, $default);
                if (!$has_translation) {
                    $domain = 'messages';
                    $has_translation = $this->checkTranslation($id, $locale, $domain, $default);
                }
            }
        }

//        $id = (string)$id;
//        while (!$catalogue->defines($id, $domain)) {
//            if ($cat = $catalogue->getFallbackCatalogue()) {
//                $catalogue = $cat;
//                $locale = $catalogue->getLocale();
//            } else {
//                break;
//            }
//        }

        if (!$has_translation && $default) {
            $translated = $default;
        } else {
            $translated = strtr($this->selector->choose($catalogue->get($id, $domain), (int)$number, $locale), $parameters);
        }

        $this->translation["found"] = $has_translation;
        $this->translation["found_in"] = $domain;

        $this->translation["translation"] = $translated;
        $this->translations[$id] = $this->translation;

        return $translated;
    }

    private function checkTranslation($id, $locale, $domain, $default = null)
    {
        $id = (string)$id;
        $catalogue = $this->getCatalogue($locale);

//        dump($catalogue); die();

        // translation in domain yml
        if ($catalogue->defines($id, $domain)) {
            $this->translations_found[$domain][] = $id;

            return true;
        }

        // translation in fallback language?
        if ($catalogue->has($id, $domain)) {
            $this->translations_fallback[$domain][] = $id;

            return true;
        }

//        dump($catalogue); die();

//        $catalogues["lv"] = $this->getCatalogue("lv");
//        $catalogues["ru"] = $this->getCatalogue("ru");
//        $catalogues["en"] = $this->getCatalogue("en");
//        dump($catalogues); die();

        // translation not found
        $this->translations_missing[$domain][] = $id;

        $rootDir = $this->symfonyContainer->getParameter('kernel.root_dir');
        $languages = [
            'lv' => [],
            'ru' => [],
            'en' => [],
        ];
        foreach ($languages as $lang => $tmp) {
            if ($domain == "FrontendBundle") {
                $file = "$rootDir/../src/C4/FrontendBundle/Resources/translations/FrontendBundle.$lang.yml";
            } else {
                $file = "$rootDir/Resources/translations/messages.$lang.yml";
            }
            $readed = [];
            $parsed = [];
            if (file_exists($file)) {
                $readed = yaml::parse(file_get_contents($file));
            }
            if ($readed) {
                foreach ($readed as $key => $val) {
                    if (is_array($val)) {
                        foreach ($this->arrayToScalar($key, $val) as $k => $v) {
                            $parsed[$k] = $v;
                        }
                    } else {
                        $parsed[$key] = $val;
                    }
                }
            }

            // copy from TranslationsBundle if exists or create new translation
            $cat = $this->getCatalogue($lang);
            $backup_domain = ($domain == "FrontendBundle") ? "TranslationBundle" : "AdminBundle";
            if ($cat->has($id, $backup_domain)) {
                $parsed[$id] = $cat->get($id, $backup_domain);
            } else {
                if ($lang == $locale && $default) {
                    $parsed[$id] = $default;
                } else {
                    $parsed[$id] = $id;
                }
            }

            $yaml = Yaml::dump($parsed, 10);
            file_put_contents($file, $yaml);

            // add to files
            $fs = new Filesystem();
            $fs->remove($this->symfonyContainer->getParameter('kernel.cache_dir')."/../dev/translations");
            $fs->remove($this->symfonyContainer->getParameter('kernel.cache_dir')."/../prod/translations");
            // add to db
            $this->check_database(($domain == "FrontendBundle") ? "fe" : "cms", $id, $parsed[$id], $lang);
        }

        // check maybe we have translation in fallback catalogue?
        $id = (string)$id;
        while (!$catalogue->defines($id, $domain)) {
            if ($cat = $catalogue->getFallbackCatalogue()) {
                $catalogue = $cat;
            } else {
                break;
            }
        }

        $this->missing_translations[$id] = [
            "id"          => $id,
            "domain"      => $domain,
            'locale'      => $locale,
            "translation" => $catalogue->get($id, $domain),
        ];

        return false;
    }

    private function check_database($context, $key, $val, $lang)
    {
        try {
            set_time_limit(0);
            ini_set("memory_limit", -1);
        } catch (\Exception $e) {
        }

        // search
        $em = $this->symfonyContainer->get('doctrine.orm.entity_manager');
        $repository = $em->getRepository('TranslationBundle:Translation');

        $db_translation = $repository->createQueryBuilder('t')
            ->where("t.context = '$context'")
//            ->andWhere("t.type = 'cms'")
            ->andWhere("t.key = '$key'")
            ->setMaxResults(1)->getQuery()->getOneOrNullResult();
        // insert
        if (!$db_translation) {
            $translation = new Translation();

            $translation->setContext($context);
            $translation->setType("text");
            $translation->setKey("$key");

            $fn = "set".ucfirst(strtolower($lang))."Translation";
            $translation->$fn($val);

            $em->persist($translation);
            $em->flush();
        } else {
            $fn = "get".ucfirst(strtolower($lang))."Translation";
            if ($db_translation->$fn() == "") {
                $fn = "set".ucfirst(strtolower($lang))."Translation";
                $db_translation->$fn($val);
                $em->persist($db_translation);
                $em->flush();
            }
        }
    }

}
