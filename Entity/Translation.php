<?php

namespace C4\TranslationBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="c4__translations")
 * @ORM\Entity
 */
class Translation 
{
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="context", type="string", length=10)
     */
    private $context;
    
    /**
     * @ORM\Column(name="type", type="string", length=10)
     */
    private $type;

    /**
     * @ORM\Column(name="_key", type="string", length=255)
     */
    private $key;

    /**
     * @ORM\Column(name="lv_translation", type="text", nullable=true)
     */
    private $lvTranslation;

    /**
     * @ORM\Column(name="ru_translation", type="text", nullable=true)
     */
    private $ruTranslation;

    /**
     * @ORM\Column(name="en_translation", type="text", nullable=true)
     */
    private $enTranslation;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set context
     *
     * @param string $context
     *
     * @return Translation
     */
    public function setContext($context)
    {
        $this->context = $context;

        return $this;
    }

    /**
     * Get context
     *
     * @return string
     */
    public function getContext()
    {
        return $this->context;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return Translation
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set key
     *
     * @param string $key
     *
     * @return Translation
     */
    public function setKey($key)
    {
        $this->key = $key;

        return $this;
    }

    /**
     * Get key
     *
     * @return string
     */
    public function getKey()
    {
        return $this->key;
    }

    /**
     * Set lvTranslation
     *
     * @param string $lvTranslation
     *
     * @return Translation
     */
    public function setLvTranslation($lvTranslation)
    {
        $this->lvTranslation = $lvTranslation;

        return $this;
    }

    /**
     * Get lvTranslation
     *
     * @return string
     */
    public function getLvTranslation()
    {
        return $this->lvTranslation;
    }

    /**
     * Set ruTranslation
     *
     * @param string $ruTranslation
     *
     * @return Translation
     */
    public function setRuTranslation($ruTranslation)
    {
        $this->ruTranslation = $ruTranslation;

        return $this;
    }

    /**
     * Get ruTranslation
     *
     * @return string
     */
    public function getRuTranslation()
    {
        return $this->ruTranslation;
    }

    /**
     * Set enTranslation
     *
     * @param string $enTranslation
     *
     * @return Translation
     */
    public function setEnTranslation($enTranslation)
    {
        $this->enTranslation = $enTranslation;

        return $this;
    }

    /**
     * Get enTranslation
     *
     * @return string
     */
    public function getEnTranslation()
    {
        return $this->enTranslation;
    }
}
