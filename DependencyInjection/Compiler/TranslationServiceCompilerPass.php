<?php namespace C4\TranslationBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Compiler\ServiceReference;
use Symfony\Component\DependencyInjection\Reference;

class TranslationServiceCompilerPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container)
    {
        $definition = $container->getDefinition('translator.default');
        $definition->setClass('C4\TranslationBundle\Components\Translator');
//        $firstArgument = $definition->getArgument(0);
//        dump($firstArgument); die();
        $definition->addArgument(new Reference('service_container'));
    }
}