<?php

namespace C4\TranslationBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use C4\TranslationBundle\DependencyInjection\Compiler\TranslationServiceCompilerPass;

class TranslationBundle extends Bundle
{
    public function build(ContainerBuilder $container)
    {
        parent::build($container);

        $container->addCompilerPass(new TranslationServiceCompilerPass());
    }
}
